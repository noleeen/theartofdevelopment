package main

import (
	"fmt"

	"github.com/mitchellh/mapstructure"
)

type Point struct{
	X int // `mapstructure: "xx"` если ключи в мапе не соответствую атрибутам структуры, то в этой команде указываем другие имена ключей
	Y int // `mapstructure: "yy"`
}

func main() {
	pointsMap := map[string]int {
		"x":1,
		"y":2,
	}

	p1 := Point{} // создаём пустую структуру чтоб взять от неё указатель, передать аргументом в функцию ниже и с пом-ю её преобразовать мапу
	mapstructure.Decode(pointsMap, &p1) // аргументами передаём мапу которую нужно преобразовать и ссылку на структуру, в которую нужно преобразовать мапу
	fmt.Println(p1) 
}