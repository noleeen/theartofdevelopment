package main

import "fmt"

type first struct {
	a, b int
}

func (f *first) Sum() int {
	sum := f.a + f.b
	return sum
}

type Interface interface {
	Sum() int
}

func main() {
	var a Interface
	f_obj := first{4, 5}
	s_obj := second{40, 50}

	a = &f_obj
	fmt.Println(a.Sum())
	a = s_obj // в этом случае можно указать и ссылкой &s_obj
	fmt.Println(a.Sum())

	var i Interface = &first{100,200}
	i.Sum()
	fmt.Printf("%v %T\n",i,i)
	
	var j Interface = second{1,2} 
	j.Sum()
	fmt.Printf("%v %T\n",j,j)

}

type second struct {
	weigth, lenght int
}

func (s second) Sum() int {
	return s.lenght - s.weigth
}
